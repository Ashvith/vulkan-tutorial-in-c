#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define CLAMP(value, lower, upper) (MIN((upper), MAX((lower), (value))))

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

const uint32_t WIDTH = 800;
const uint32_t HEIGHT = 600;
const int MAX_FRAMES_IN_FLIGHT = 2;

const char *validationLayers[] = {"VK_LAYER_KHRONOS_validation"};
const uint32_t validationLayerCount =
    sizeof(validationLayers) / sizeof(*validationLayers);

const char *deviceExtensions[] = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
const uint32_t deviceExtensionCount =
    sizeof(deviceExtensions) / sizeof(*deviceExtensions);

#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

typedef struct App {
  GLFWwindow *window;
  VkInstance instance;
  VkDebugUtilsMessengerEXT debugMessenger;
  VkSurfaceKHR surface;
  VkPhysicalDevice physicalDevice;
  VkDevice device;
  VkQueue graphicsQueue;
  VkQueue presentQueue;
  VkSwapchainKHR swapChain;
  VkImage *swapChainImages;
  uint32_t swapChainImageCount;
  VkFormat swapChainImageFormat;
  VkExtent2D swapChainExtent;
  VkImageView *swapChainImageViews;
  VkRenderPass renderPass;
  VkPipelineLayout pipelineLayout;
  VkPipeline graphicsPipeline;
  VkFramebuffer *swapChainFramebuffers;
  VkCommandPool commandPool;
  VkCommandBuffer *commandBuffers;
  VkSemaphore *imageAvailableSemaphore;
  VkSemaphore *renderFinishedSemaphore;
  VkFence *inFlightFence;
  uint32_t currentFrame;
} App;

typedef struct QueueFamilyIndices {
  uint32_t graphicsFamily;
  bool isGraphicsFamilySet;
  uint32_t presentFamily;
  bool isPresentFamilySet;
} QueueFamilyIndices;

typedef struct SwapChainSupportDetails {
  VkSurfaceCapabilitiesKHR capabilities;
  VkSurfaceFormatKHR *formats;
  uint32_t formatCount;
  VkPresentModeKHR *presentModes;
  uint32_t presentModeCount;
} SwapChainSupportDetails;

typedef struct ShaderFile {
  char *code;
  uint32_t codeCount;
} ShaderFile;

void initWindow(App *pApp);
void initVulkan(App *pApp);
void mainLoop(App *pApp);
void cleanup(App *pApp);
void createInstance(App *pApp);
bool checkValidationLayerSupport(void);
const char **getRequiredExtensions(uint32_t *extensionCount);
static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData, void *pUserData);
void setupDebugMessenger(App *pApp);
VkResult CreateDebugUtilsMessengerEXT(
    VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT *pCreateInfo,
    const VkAllocationCallbacks *pAllocator,
    VkDebugUtilsMessengerEXT *pDebugMessenger);
void DestroyDebugUtilsMessengerEXT(VkInstance instance,
                                   VkDebugUtilsMessengerEXT debugMessenger,
                                   const VkAllocationCallbacks *pAllocator);
void populateDebugMessengerCreateInfo(
    VkDebugUtilsMessengerCreateInfoEXT *createInfo);
void pickPhysicalDevice(App *pApp);
bool isDeviceSuitable(VkPhysicalDevice device, VkSurfaceKHR surface);
QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device,
                                     VkSurfaceKHR surface);
void createLogicalDevice(App *pApp);
void createSurface(App *pApp);
bool isComplete(QueueFamilyIndices indices);
bool checkDeviceExtensionSupport(VkPhysicalDevice device);
SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device,
                                              VkSurfaceKHR surface);
VkSurfaceFormatKHR
chooseSwapSurfaceFormat(const VkSurfaceFormatKHR *availableFormats,
                        uint32_t formatCount);
VkPresentModeKHR
chooseSwapPresentMode(const VkPresentModeKHR *availablePresentModes,
                      uint32_t presentModeCount);
VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR capabilities,
                            GLFWwindow *window);
void createSwapChain(App *pApp);
void createImageViews(App *pApp);
void createGraphicsPipeline(App *pApp);
ShaderFile readFile(const char *filename);
VkShaderModule createShaderModule(App *pApp, ShaderFile *shaderFile);
void createRenderPass(App *pApp);
void createFramebuffers(App *pApp);
void createCommandPool(App *pApp);
void createCommandBuffer(App *pApp);
void recordCommandBuffer(App *pApp, uint32_t imageIndex);
void drawFrame(App *pApp);
void createSyncObjects(App *pApp);
void createCommandBuffers(App *pApp);
