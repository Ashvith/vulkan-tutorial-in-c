#include "main.h"

int main(int argc, char **argv) {
  App app = {0};

  initWindow(&app);
  initVulkan(&app);
  mainLoop(&app);
  cleanup(&app);

  return 0;
}

void initWindow(App *pApp) {
  glfwInit();

  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

  pApp->window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan", NULL, NULL);
}

void initVulkan(App *pApp) {
  createInstance(pApp);
  setupDebugMessenger(pApp);
  createSurface(pApp);
  pickPhysicalDevice(pApp);
  createLogicalDevice(pApp);
  createSwapChain(pApp);
  createImageViews(pApp);
  createRenderPass(pApp);
  createGraphicsPipeline(pApp);
  createFramebuffers(pApp);
  createCommandPool(pApp);
  createCommandBuffers(pApp);
  createSyncObjects(pApp);
}

void mainLoop(App *pApp) {
  while (!glfwWindowShouldClose(pApp->window)) {
    glfwPollEvents();
    drawFrame(pApp);
  }

  vkDeviceWaitIdle(pApp->device);
}

void cleanup(App *pApp) {
  for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
    vkDestroySemaphore(pApp->device, pApp->imageAvailableSemaphore[i], NULL);
    vkDestroySemaphore(pApp->device, pApp->renderFinishedSemaphore[i], NULL);
    vkDestroyFence(pApp->device, pApp->inFlightFence[i], NULL);
  }

  vkDestroyCommandPool(pApp->device, pApp->commandPool, NULL);

  for (int i = 0; i < pApp->swapChainImageCount; ++i) {
    vkDestroyFramebuffer(pApp->device, pApp->swapChainFramebuffers[i], NULL);
  }

  vkDestroyPipeline(pApp->device, pApp->graphicsPipeline, NULL);
  vkDestroyPipelineLayout(pApp->device, pApp->pipelineLayout, NULL);
  vkDestroyRenderPass(pApp->device, pApp->renderPass, NULL);

  for (size_t i = 0; i < pApp->swapChainImageCount; ++i) {
    vkDestroyImageView(pApp->device, pApp->swapChainImageViews[i], NULL);
  }

  vkDestroySwapchainKHR(pApp->device, pApp->swapChain, NULL);
  vkDestroyDevice(pApp->device, NULL);

  if (enableValidationLayers) {
    DestroyDebugUtilsMessengerEXT(pApp->instance, pApp->debugMessenger, NULL);
  }

  vkDestroySurfaceKHR(pApp->instance, pApp->surface, NULL);
  vkDestroyInstance(pApp->instance, NULL);

  glfwDestroyWindow(pApp->window);

  glfwTerminate();
}

void createInstance(App *pApp) {
  if (enableValidationLayers && !checkValidationLayerSupport()) {
    fprintf(stderr, "Validation layers requested, but not available!\n");
    abort();
  }

  VkApplicationInfo appInfo = {.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
                               .pApplicationName = "Hello Triangle",
                               .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
                               .pEngineName = "No Engine",
                               .engineVersion = VK_MAKE_VERSION(1, 0, 0),
                               .apiVersion = VK_API_VERSION_1_0,
                               .pNext = NULL};

  uint32_t extensionCount = 0;
  const char **extensions = getRequiredExtensions(&extensionCount);

  VkInstanceCreateInfo createInfo = {.sType =
                                         VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
                                     .pApplicationInfo = &appInfo,
                                     .enabledExtensionCount = extensionCount,
                                     .ppEnabledExtensionNames = extensions};
  VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo = {0};

  if (enableValidationLayers) {
    createInfo.enabledLayerCount = validationLayerCount;
    createInfo.ppEnabledLayerNames = validationLayers;
    populateDebugMessengerCreateInfo(&debugCreateInfo);
    createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT *)&debugCreateInfo;
  } else {
    createInfo.enabledLayerCount = 0;
    createInfo.pNext = NULL;
  }

  if (vkCreateInstance(&createInfo, NULL, &pApp->instance) != VK_SUCCESS) {
    fprintf(stderr, "Failed to create instance!\n");
    abort();
  }
}

bool checkValidationLayerSupport(void) {
  uint32_t layerCount;
  vkEnumerateInstanceLayerProperties(&layerCount, NULL);
  VkLayerProperties availableLayers[layerCount];
  vkEnumerateInstanceLayerProperties(&layerCount, availableLayers);

  for (size_t i = 0; i < validationLayerCount; ++i) {
    bool layerFound = false;

    for (size_t j = 0; j < layerCount; ++j) {
      if (strcmp(validationLayers[i], availableLayers[j].layerName) == 0) {
        layerFound = true;
        break;
      }
    }

    if (!layerFound) {
      return false;
    }
  }
  return true;
}

const char **getRequiredExtensions(uint32_t *extensionCount) {
  uint32_t glfwExtensionCount = 0;
  const char **glfwExtensions;

  glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

  const char **extensions =
      (const char **)malloc(glfwExtensionCount * sizeof(const char *));
  memcpy(extensions, glfwExtensions, glfwExtensionCount * sizeof(const char *));

  if (enableValidationLayers) {
    extensions = (const char **)realloc(extensions, glfwExtensionCount *
                                                        sizeof(const char *));
    extensions[glfwExtensionCount] = VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
    ++glfwExtensionCount;
  }

  *extensionCount = glfwExtensionCount;
  return extensions;
}

static VKAPI_ATTR VkBool32 VKAPI_CALL
debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
              VkDebugUtilsMessageTypeFlagsEXT messageType,
              const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
              void *pUserData) {
  fprintf(stderr, "Validation layer: %s\n", pCallbackData->pMessage);
  return VK_FALSE;
}

void setupDebugMessenger(App *pApp) {
  if (!enableValidationLayers)
    return;

  VkDebugUtilsMessengerCreateInfoEXT createInfo;
  populateDebugMessengerCreateInfo(&createInfo);

  if (CreateDebugUtilsMessengerEXT(pApp->instance, &createInfo, NULL,
                                   &pApp->debugMessenger) != VK_SUCCESS) {
    fprintf(stderr, "Failed to set up debug messenger!\n");
    abort();
  }
}

VkResult CreateDebugUtilsMessengerEXT(
    VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT *pCreateInfo,
    const VkAllocationCallbacks *pAllocator,
    VkDebugUtilsMessengerEXT *pDebugMessenger) {
  PFN_vkCreateDebugUtilsMessengerEXT func =
      (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
          instance, "vkCreateDebugUtilsMessengerEXT");
  if (func != NULL) {
    return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
  } else {
    return VK_ERROR_EXTENSION_NOT_PRESENT;
  }
}

void DestroyDebugUtilsMessengerEXT(VkInstance instance,
                                   VkDebugUtilsMessengerEXT debugMessenger,
                                   const VkAllocationCallbacks *pAllocator) {
  PFN_vkDestroyDebugUtilsMessengerEXT func =
      (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
          instance, "vkDestroyDebugUtilsMessengerEXT");
  if (func != NULL) {
    func(instance, debugMessenger, pAllocator);
  }
}

void populateDebugMessengerCreateInfo(
    VkDebugUtilsMessengerCreateInfoEXT *createInfo) {
  *createInfo = (VkDebugUtilsMessengerCreateInfoEXT){
      .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
      .messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                         VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                         VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
      .messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                     VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                     VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
      .pfnUserCallback = debugCallback};
}

void pickPhysicalDevice(App *pApp) {
  uint32_t deviceCount = 0;
  vkEnumeratePhysicalDevices(pApp->instance, &deviceCount, NULL);

  if (deviceCount == 0) {
    fprintf(stderr, "Failed to find GPUs with Vulkan support!\n");
    abort();
  }

  VkPhysicalDevice devices[deviceCount];
  vkEnumeratePhysicalDevices(pApp->instance, &deviceCount, devices);

  for (size_t i = 0; i < deviceCount; ++i) {
    if (isDeviceSuitable(devices[i], pApp->surface)) {
      pApp->physicalDevice = devices[i];
      break;
    }
  }

  if (pApp->physicalDevice == VK_NULL_HANDLE) {
    fprintf(stderr, "Failed to find a suitable GPU!\n");
    abort();
  }

  VkPhysicalDeviceProperties deviceProperties;
  vkGetPhysicalDeviceProperties(pApp->physicalDevice, &deviceProperties);
  printf("Device selected: %s\n", deviceProperties.deviceName);
}

bool isDeviceSuitable(VkPhysicalDevice device, VkSurfaceKHR surface) {
  QueueFamilyIndices indices = findQueueFamilies(device, surface);

  bool extensionsSupported = checkDeviceExtensionSupport(device);

  bool swapChainAdequate = false;
  if (extensionsSupported) {
    SwapChainSupportDetails swapChainSupport =
        querySwapChainSupport(device, surface);
    swapChainAdequate = !(swapChainSupport.formats == NULL) &&
                        !(swapChainSupport.presentModes == NULL);
  }

  return isComplete(indices) && extensionsSupported && swapChainAdequate;
}

QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device,
                                     VkSurfaceKHR surface) {
  QueueFamilyIndices indices;

  uint32_t queueFamilyCount = 0;
  vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, NULL);

  VkQueueFamilyProperties queueFamilies[queueFamilyCount];
  vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount,
                                           queueFamilies);

  for (size_t i = 0; i < queueFamilyCount; ++i) {
    if (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
      indices.graphicsFamily = (uint32_t)i;
      indices.isGraphicsFamilySet = true;
    }

    VkBool32 presentSupport = false;
    vkGetPhysicalDeviceSurfaceSupportKHR(device, (uint32_t)i, surface,
                                         &presentSupport);

    if (presentSupport) {
      indices.presentFamily = (uint32_t)i;
      indices.isPresentFamilySet = true;
    }

    if (isComplete(indices)) {
      break;
    }
  }

  return indices;
}

void createLogicalDevice(App *pApp) {
  QueueFamilyIndices indices =
      findQueueFamilies(pApp->physicalDevice, pApp->surface);
  float queuePriority = 1.0f;

  uint32_t uniqueQueueFamiliesCount =
      sizeof(QueueFamilyIndices) / (2 * sizeof(uint32_t));

  // Due to padding in structure, size of bool will be equal to uint32_t, so
  // we'll make use if 2 * sizeof(uint32_t) instead of sizeof(uint32_t) +
  // sizeof(bool)

  uint32_t *uniqueQueueFamilies =
      (uint32_t *)malloc(uniqueQueueFamiliesCount * sizeof(uint32_t));
  uniqueQueueFamilies[0] = indices.graphicsFamily;
  uniqueQueueFamilies[1] = indices.presentFamily;

  for (size_t i = uniqueQueueFamiliesCount - 1; i > 0; --i) {
    for (size_t j = i - 1; j >= 0; --j) {
      if (uniqueQueueFamilies[i] == uniqueQueueFamilies[j]) {
        uniqueQueueFamiliesCount -= 1;
        uniqueQueueFamilies = (uint32_t *)realloc(
            uniqueQueueFamilies, uniqueQueueFamiliesCount * sizeof(uint32_t));
        break;
      }
    }
  }

  VkDeviceQueueCreateInfo *queueCreateInfos = (VkDeviceQueueCreateInfo *)malloc(
      (uint32_t)uniqueQueueFamiliesCount * sizeof(VkDeviceQueueCreateInfo));

  for (size_t i = 0; i < uniqueQueueFamiliesCount; ++i) {
    queueCreateInfos[i] = (VkDeviceQueueCreateInfo){
        .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        .queueFamilyIndex = uniqueQueueFamilies[i],
        .queueCount = 1,
        .pQueuePriorities = &queuePriority};
  }

  VkPhysicalDeviceFeatures deviceFeatures = {};

  VkDeviceCreateInfo createInfo = {
      .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
      .pQueueCreateInfos = queueCreateInfos,
      .queueCreateInfoCount = (uint32_t)uniqueQueueFamiliesCount,
      .pEnabledFeatures = &deviceFeatures,
      .enabledExtensionCount = deviceExtensionCount,
      .ppEnabledExtensionNames = deviceExtensions};

  if (enableValidationLayers) {
    createInfo.enabledLayerCount = validationLayerCount;
    createInfo.ppEnabledLayerNames = validationLayers;
  } else {
    createInfo.enabledLayerCount = 0;
  }

  free(uniqueQueueFamilies);

  if (vkCreateDevice(pApp->physicalDevice, &createInfo, NULL, &pApp->device) !=
      VK_SUCCESS) {
    fprintf(stderr, "Failed to create logical device!\n");
    abort();
  }
  vkGetDeviceQueue(pApp->device, indices.graphicsFamily, 0,
                   &pApp->graphicsQueue);
  vkGetDeviceQueue(pApp->device, indices.presentFamily, 0, &pApp->presentQueue);
}

void createSurface(App *pApp) {
  if (glfwCreateWindowSurface(pApp->instance, pApp->window, NULL,
                              &pApp->surface) != VK_SUCCESS) {
    fprintf(stderr, "Failed to create a window surface!\n");
    abort();
  }
}

bool isComplete(QueueFamilyIndices indices) {
  return indices.isGraphicsFamilySet && indices.isPresentFamilySet;
}

bool checkDeviceExtensionSupport(VkPhysicalDevice device) {
  uint32_t extensionCount;
  vkEnumerateDeviceExtensionProperties(device, NULL, &extensionCount, NULL);

  VkExtensionProperties availableExtensions[extensionCount];
  vkEnumerateDeviceExtensionProperties(device, NULL, &extensionCount,
                                       availableExtensions);

  uint32_t requiredExtensionsCount = deviceExtensionCount;
  char **requiredExtensions =
      (char **)malloc(requiredExtensionsCount * sizeof(char *));

  for (size_t i = 0; i < requiredExtensionsCount; ++i) {
    requiredExtensions[i] = strdup(deviceExtensions[i]);
  }

  for (size_t i = requiredExtensionsCount - 1; i > 0; --i) {
    for (size_t j = i - 1; j >= 0; --j) {
      if (strcmp(requiredExtensions[i], requiredExtensions[j]) == 0) {
        requiredExtensionsCount -= 1;
        free(requiredExtensions[i]);
        requiredExtensions = (char **)realloc(
            requiredExtensions, requiredExtensionsCount * sizeof(char *));
        break;
      }
    }
  }

  for (size_t i = 0; i < extensionCount; ++i) {
    for (ssize_t j = requiredExtensionsCount - 1;
         requiredExtensionsCount > 0 && j > -1; --j) {
      if (strcmp(availableExtensions[i].extensionName, requiredExtensions[j]) ==
          0) {
        requiredExtensionsCount -= 1;
        free(requiredExtensions[j]);
        requiredExtensions = (char **)realloc(
            requiredExtensions, requiredExtensionsCount * sizeof(char *));
        break;
      }
    }
  }

  for (size_t i = 0; i < requiredExtensionsCount; ++i) {
    free(requiredExtensions[i]);
  }
  free(requiredExtensions);

  return requiredExtensionsCount == 0;
}

SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device,
                                              VkSurfaceKHR surface) {
  SwapChainSupportDetails details;

  vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface,
                                            &details.capabilities);

  vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &details.formatCount,
                                       NULL);
  if (details.formatCount != 0) {
    details.formats = (VkSurfaceFormatKHR *)malloc(details.formatCount *
                                                   sizeof(VkSurfaceFormatKHR));
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &details.formatCount,
                                         details.formats);
  }

  vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface,
                                            &details.presentModeCount, NULL);
  if (details.presentModeCount != 0) {
    details.presentModes = (VkPresentModeKHR *)malloc(details.presentModeCount *
                                                      sizeof(VkPresentModeKHR));
    vkGetPhysicalDeviceSurfacePresentModesKHR(
        device, surface, &details.presentModeCount, details.presentModes);
  }

  return details;
}

VkSurfaceFormatKHR
chooseSwapSurfaceFormat(const VkSurfaceFormatKHR *availableFormats,
                        uint32_t formatCount) {
  for (size_t i = 0; i < formatCount; ++i) {
    if (availableFormats[i].format == VK_FORMAT_B8G8R8A8_SRGB &&
        availableFormats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
      return availableFormats[i];
    }
  }
  return availableFormats[0];
}

VkPresentModeKHR
chooseSwapPresentMode(const VkPresentModeKHR *availablePresentModes,
                      uint32_t presentModeCount) {
  for (size_t i = 0; i < presentModeCount; ++i) {
    if (availablePresentModes[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
      return availablePresentModes[i];
    }
  }
  return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR capabilities,
                            GLFWwindow *window) {
  if (capabilities.currentExtent.width != UINT32_MAX) {
    return capabilities.currentExtent;
  } else {
    uint32_t width, height;
    glfwGetFramebufferSize(window, &width, &height);

    VkExtent2D actualExtent = {width, height};

    actualExtent.width =
        CLAMP(actualExtent.width, capabilities.minImageExtent.width,
              capabilities.maxImageExtent.width);
    actualExtent.height =
        CLAMP(actualExtent.height, capabilities.minImageExtent.height,
              capabilities.maxImageExtent.height);

    return actualExtent;
  }
}

void createSwapChain(App *pApp) {
  SwapChainSupportDetails swapChainSupport =
      querySwapChainSupport(pApp->physicalDevice, pApp->surface);
  VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(
      swapChainSupport.formats, swapChainSupport.formatCount);
  VkPresentModeKHR presentMode = chooseSwapPresentMode(
      swapChainSupport.presentModes, swapChainSupport.presentModeCount);
  VkExtent2D extent =
      chooseSwapExtent(swapChainSupport.capabilities, pApp->window);

  pApp->swapChainImageCount = swapChainSupport.capabilities.minImageCount + 1;
  if (swapChainSupport.capabilities.maxImageCount > 0 &&
      pApp->swapChainImageCount > swapChainSupport.capabilities.maxImageCount) {
    pApp->swapChainImageCount = swapChainSupport.capabilities.maxImageCount;
  }

  VkSwapchainCreateInfoKHR createInfo = {
      .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
      .surface = pApp->surface,
      .minImageCount = pApp->swapChainImageCount,
      .imageFormat = surfaceFormat.format,
      .imageColorSpace = surfaceFormat.colorSpace,
      .imageExtent = extent,
      .imageArrayLayers = 1,
      .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT};

  QueueFamilyIndices indices =
      findQueueFamilies(pApp->physicalDevice, pApp->surface);
  uint32_t queueFamilyIndices[] = {indices.graphicsFamily,
                                   indices.presentFamily};

  if (indices.graphicsFamily != indices.presentFamily) {
    createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
    createInfo.queueFamilyIndexCount = 2;
    createInfo.pQueueFamilyIndices = queueFamilyIndices;
  } else {
    createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    createInfo.queueFamilyIndexCount = 0;
    createInfo.pQueueFamilyIndices = NULL;
  }

  createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
  createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
  createInfo.presentMode = presentMode;
  createInfo.clipped = VK_TRUE;
  createInfo.oldSwapchain = VK_NULL_HANDLE;

  if (vkCreateSwapchainKHR(pApp->device, &createInfo, NULL, &pApp->swapChain) !=
      VK_SUCCESS) {
    fprintf(stderr, "Failed to create swap chain\n");
    abort();
  }

  vkGetSwapchainImagesKHR(pApp->device, pApp->swapChain,
                          &pApp->swapChainImageCount, NULL);
  pApp->swapChainImages =
      (VkImage *)malloc(pApp->swapChainImageCount * sizeof(VkImage));
  vkGetSwapchainImagesKHR(pApp->device, pApp->swapChain,
                          &pApp->swapChainImageCount, pApp->swapChainImages);

  pApp->swapChainImageFormat = surfaceFormat.format;
  pApp->swapChainExtent = extent;
}

void createImageViews(App *pApp) {
  pApp->swapChainImageViews =
      (VkImageView *)malloc(pApp->swapChainImageCount * sizeof(VkImageView));

  for (size_t i = 0; i < pApp->swapChainImageCount; ++i) {
    VkImageViewCreateInfo createInfo = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = pApp->swapChainImages[i],
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = pApp->swapChainImageFormat,
        .components = {.r = VK_COMPONENT_SWIZZLE_IDENTITY,
                       .g = VK_COMPONENT_SWIZZLE_IDENTITY,
                       .b = VK_COMPONENT_SWIZZLE_IDENTITY,
                       .a = VK_COMPONENT_SWIZZLE_IDENTITY},
        .subresourceRange = {.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                             .baseMipLevel = 0,
                             .levelCount = 1,
                             .baseArrayLayer = 0,
                             .layerCount = 1}};

    if (vkCreateImageView(pApp->device, &createInfo, NULL,
                          &pApp->swapChainImageViews[i]) != VK_SUCCESS) {
      fprintf(stderr, "Failed to create image view!\n");
      abort();
    }
  }
}

void createGraphicsPipeline(App *pApp) {
  ShaderFile vertShaderFile = readFile("./build/vert.spv");
  ShaderFile fragShaderFile = readFile("./build/frag.spv");

  VkShaderModule vertShaderModule = createShaderModule(pApp, &vertShaderFile);
  VkShaderModule fragShaderModule = createShaderModule(pApp, &fragShaderFile);

  VkPipelineShaderStageCreateInfo vertShaderStageInfo = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
      .stage = VK_SHADER_STAGE_VERTEX_BIT,
      .module = vertShaderModule,
      .pName = "main"};

  VkPipelineShaderStageCreateInfo fragShaderStageInfo = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
      .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
      .module = fragShaderModule,
      .pName = "main"};

  VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderStageInfo,
                                                    fragShaderStageInfo};

  VkDynamicState dynamicStates[] = {VK_DYNAMIC_STATE_VIEWPORT,
                                    VK_DYNAMIC_STATE_SCISSOR};
  uint32_t dynamicStateCount = sizeof(dynamicStates) / sizeof(VkDynamicState);

  VkPipelineDynamicStateCreateInfo dynamicState = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
      .dynamicStateCount = dynamicStateCount,
      .pDynamicStates = dynamicStates};

  VkPipelineVertexInputStateCreateInfo vertexInputInfo = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
      .vertexBindingDescriptionCount = 0,
      .pVertexBindingDescriptions = NULL,
      .vertexAttributeDescriptionCount = 0,
      .pVertexAttributeDescriptions = NULL};

  VkPipelineInputAssemblyStateCreateInfo inputAssembly = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
      .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
      .primitiveRestartEnable = VK_FALSE};

  VkPipelineViewportStateCreateInfo viewportState = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
      .viewportCount = 1,
      .scissorCount = 1};

  VkPipelineRasterizationStateCreateInfo rasterizer = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
      .depthClampEnable = VK_FALSE,
      .rasterizerDiscardEnable = VK_FALSE,
      .polygonMode = VK_POLYGON_MODE_FILL,
      .lineWidth = 1.0f,
      .cullMode = VK_CULL_MODE_BACK_BIT,
      .frontFace = VK_FRONT_FACE_CLOCKWISE,
      .depthBiasEnable = VK_FALSE,
      .depthBiasConstantFactor = 0.0f,
      .depthBiasClamp = 0.0f,
      .depthBiasSlopeFactor = 0.0f};

  VkPipelineMultisampleStateCreateInfo multisampling = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
      .sampleShadingEnable = VK_FALSE,
      .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
      .minSampleShading = 1.0f,
      .pSampleMask = NULL,
      .alphaToCoverageEnable = VK_FALSE,
      .alphaToOneEnable = VK_FALSE};

  VkPipelineColorBlendAttachmentState colorBlendAttachment = {
      .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
                        VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
      .blendEnable = VK_FALSE,
      .srcColorBlendFactor = VK_BLEND_FACTOR_ONE,
      .dstColorBlendFactor = VK_BLEND_FACTOR_ZERO,
      .colorBlendOp = VK_BLEND_OP_ADD,
      .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
      .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
      .alphaBlendOp = VK_BLEND_OP_ADD};

  VkPipelineColorBlendStateCreateInfo colorBlending = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
      .logicOpEnable = VK_FALSE,
      .logicOp = VK_LOGIC_OP_COPY,
      .attachmentCount = 1,
      .pAttachments = &colorBlendAttachment,
      .blendConstants = {0.0f, 0.0f, 0.0f, 0.0f}};

  VkPipelineLayoutCreateInfo pipelineLayoutInfo = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
      .setLayoutCount = 0,
      .pSetLayouts = NULL,
      .pushConstantRangeCount = 0,
      .pPushConstantRanges = NULL};

  if (vkCreatePipelineLayout(pApp->device, &pipelineLayoutInfo, NULL,
                             &pApp->pipelineLayout) != VK_SUCCESS) {
    fprintf(stderr, "Failed to create pipeline layout\n");
    abort();
  }

  VkGraphicsPipelineCreateInfo pipelineInfo = {
      .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
      .stageCount = 2,
      .pStages = shaderStages,
      .pVertexInputState = &vertexInputInfo,
      .pInputAssemblyState = &inputAssembly,
      .pViewportState = &viewportState,
      .pRasterizationState = &rasterizer,
      .pMultisampleState = &multisampling,
      .pDepthStencilState = NULL,
      .pColorBlendState = &colorBlending,
      .pDynamicState = &dynamicState,
      .layout = pApp->pipelineLayout,
      .renderPass = pApp->renderPass,
      .subpass = 0,
      .basePipelineHandle = VK_NULL_HANDLE,
      .basePipelineIndex = -1};

  if (vkCreateGraphicsPipelines(pApp->device, VK_NULL_HANDLE, 1, &pipelineInfo,
                                NULL, &pApp->graphicsPipeline) != VK_SUCCESS) {
    fprintf(stderr, "Failed to create graphics pipeline!\n");
    abort();
  }

  vkDestroyShaderModule(pApp->device, fragShaderModule, NULL);
  vkDestroyShaderModule(pApp->device, vertShaderModule, NULL);
}

ShaderFile readFile(const char *filename) {
  FILE *pFile = fopen(filename, "rb");

  if (pFile == NULL) {
    fprintf(stderr, "Failed to open file!\n");
    fclose(pFile);
    abort();
  }

  fseek(pFile, SEEK_SET, SEEK_END);
  size_t fileSize = ftell(pFile);
  fseek(pFile, SEEK_SET, SEEK_SET);

  char *buffer = (char *)malloc((fileSize) * sizeof(char));
  fread(buffer, sizeof(char), fileSize, pFile);

  fclose(pFile);
  return (ShaderFile){.codeCount = fileSize, .code = buffer};
}

VkShaderModule createShaderModule(App *pApp, ShaderFile *shaderFile) {
  VkShaderModuleCreateInfo createInfo = {
      .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
      .codeSize = shaderFile->codeCount,
      .pCode = (uint32_t *)shaderFile->code};

  VkShaderModule shaderModule;
  if (vkCreateShaderModule(pApp->device, &createInfo, NULL, &shaderModule) !=
      VK_SUCCESS) {
    fprintf(stderr, "Failed to create shader module!\n");
    abort();
  }

  return shaderModule;
}

void createRenderPass(App *pApp) {
  VkAttachmentDescription colorAttachment = {
      .format = pApp->swapChainImageFormat,
      .samples = VK_SAMPLE_COUNT_1_BIT,
      .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
      .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
      .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
      .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
      .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
      .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR};

  VkAttachmentReference colorAttachmentRef = {
      .attachment = 0, .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL};

  VkSubpassDescription subpass = {.pipelineBindPoint =
                                      VK_PIPELINE_BIND_POINT_GRAPHICS,
                                  .colorAttachmentCount = 1,
                                  .pColorAttachments = &colorAttachmentRef};

  VkSubpassDependency dependency = {
      .srcSubpass = VK_SUBPASS_EXTERNAL,
      .dstSubpass = 0,
      .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
      .srcAccessMask = 0,
      .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
      .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT};

  VkRenderPassCreateInfo renderPassInfo = {
      .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
      .attachmentCount = 1,
      .pAttachments = &colorAttachment,
      .subpassCount = 1,
      .pSubpasses = &subpass,
      .dependencyCount = 1,
      .pDependencies = &dependency};

  if (vkCreateRenderPass(pApp->device, &renderPassInfo, NULL,
                         &pApp->renderPass) != VK_SUCCESS) {
    fprintf(stderr, "Failed to create render pass!\n");
    abort();
  }
}

void createFramebuffers(App *pApp) {
  pApp->swapChainFramebuffers = (VkFramebuffer *)malloc(
      pApp->swapChainImageCount * sizeof(VkFramebuffer));

  for (size_t i = 0; i < pApp->swapChainImageCount; ++i) {
    VkImageView attachments[] = {pApp->swapChainImageViews[i]};
    VkFramebufferCreateInfo framebufferInfo = {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .renderPass = pApp->renderPass,
        .attachmentCount = 1,
        .pAttachments = attachments,
        .width = pApp->swapChainExtent.width,
        .height = pApp->swapChainExtent.height,
        .layers = 1};

    if (vkCreateFramebuffer(pApp->device, &framebufferInfo, NULL,
                            &pApp->swapChainFramebuffers[i]) != VK_SUCCESS) {
      fprintf(stderr, "Failed to create framebuffer!\n");
      abort();
    }
  }
}

void createCommandPool(App *pApp) {
  QueueFamilyIndices queueFamilyIndices =
      findQueueFamilies(pApp->physicalDevice, pApp->surface);
  VkCommandPoolCreateInfo poolInfo = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
      .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
      .queueFamilyIndex = queueFamilyIndices.graphicsFamily};

  if (vkCreateCommandPool(pApp->device, &poolInfo, NULL, &pApp->commandPool) !=
      VK_SUCCESS) {
    fprintf(stderr, "Failed to create command pool!\n");
    abort();
  }
}

void createCommandBuffers(App *pApp) {
  pApp->commandBuffers =
      (VkCommandBuffer *)malloc(MAX_FRAMES_IN_FLIGHT * sizeof(VkCommandBuffer));

  VkCommandBufferAllocateInfo allocInfo = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
      .commandPool = pApp->commandPool,
      .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
      .commandBufferCount = MAX_FRAMES_IN_FLIGHT};

  if (vkAllocateCommandBuffers(pApp->device, &allocInfo,
                               pApp->commandBuffers) != VK_SUCCESS) {
    fprintf(stderr, "Failed to allocate command buffers!\n");
    abort();
  }
}

void recordCommandBuffer(App *pApp, uint32_t imageIndex) {
  VkCommandBufferBeginInfo beginInfo = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
      .flags = 0,
      .pInheritanceInfo = NULL};

  if (vkBeginCommandBuffer(pApp->commandBuffers[pApp->currentFrame],
                           &beginInfo) != VK_SUCCESS) {
    fprintf(stderr, "Failed to begin recording command buffer!\n");
    abort();
  }

  VkClearValue clearColor = {{{0.0f, 0.0f, 0.0f, 1.0f}}};

  VkRenderPassBeginInfo renderPassInfo = {
      .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
      .renderPass = pApp->renderPass,
      .framebuffer = pApp->swapChainFramebuffers[imageIndex],
      .renderArea = {.offset = {0, 0}, .extent = pApp->swapChainExtent},
      .clearValueCount = 1,
      .pClearValues = &clearColor};

  vkCmdBeginRenderPass(pApp->commandBuffers[pApp->currentFrame],
                       &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

  vkCmdBindPipeline(pApp->commandBuffers[pApp->currentFrame],
                    VK_PIPELINE_BIND_POINT_GRAPHICS, pApp->graphicsPipeline);
  VkViewport viewport = {.x = 0.0f,
                         .y = 0.0f,
                         .width = (float)pApp->swapChainExtent.width,
                         .height = (float)pApp->swapChainExtent.height,
                         .minDepth = 0.0f,
                         .maxDepth = 1.0f};
  vkCmdSetViewport(pApp->commandBuffers[pApp->currentFrame], 0, 1, &viewport);

  VkRect2D scissor = {.offset = {0, 0}, .extent = pApp->swapChainExtent};
  vkCmdSetScissor(pApp->commandBuffers[pApp->currentFrame], 0, 1, &scissor);

  vkCmdDraw(pApp->commandBuffers[pApp->currentFrame], 3, 1, 0, 0);
  vkCmdEndRenderPass(pApp->commandBuffers[pApp->currentFrame]);

  if (vkEndCommandBuffer(pApp->commandBuffers[pApp->currentFrame]) !=
      VK_SUCCESS) {
    fprintf(stderr, "Failed to record command buffer!\n");
    abort();
  }
}

void drawFrame(App *pApp) {
  vkWaitForFences(pApp->device, 1, &pApp->inFlightFence[pApp->currentFrame],
                  VK_TRUE, UINT64_MAX);
  vkResetFences(pApp->device, 1, &pApp->inFlightFence[pApp->currentFrame]);

  uint32_t imageIndex;
  vkAcquireNextImageKHR(pApp->device, pApp->swapChain, UINT64_MAX,
                        pApp->imageAvailableSemaphore[pApp->currentFrame],
                        VK_NULL_HANDLE, &imageIndex);

  vkResetCommandBuffer(pApp->commandBuffers[pApp->currentFrame], 0);
  recordCommandBuffer(pApp, imageIndex);

  VkSemaphore waitSemaphores[] = {
      pApp->imageAvailableSemaphore[pApp->currentFrame]};
  VkPipelineStageFlags waitStages[] = {
      VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
  VkSemaphore signalSemaphores[] = {
      pApp->renderFinishedSemaphore[pApp->currentFrame]};

  VkSubmitInfo submitInfo = {.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                             .waitSemaphoreCount = 1,
                             .pWaitSemaphores = waitSemaphores,
                             .pWaitDstStageMask = waitStages,
                             .commandBufferCount = 1,
                             .pCommandBuffers =
                                 &pApp->commandBuffers[pApp->currentFrame],
                             .signalSemaphoreCount = 1,
                             .pSignalSemaphores = signalSemaphores};

  if (vkQueueSubmit(pApp->graphicsQueue, 1, &submitInfo,
                    pApp->inFlightFence[pApp->currentFrame]) != VK_SUCCESS) {
    fprintf(stderr, "Failed to submit draw command buffer!\n");
    abort();
  }

  VkSwapchainKHR swapChains[] = {pApp->swapChain};
  VkPresentInfoKHR presentInfo = {.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
                                  .waitSemaphoreCount = 1,
                                  .pWaitSemaphores = signalSemaphores,
                                  .swapchainCount = 1,
                                  .pSwapchains = swapChains,
                                  .pImageIndices = &imageIndex,
                                  .pResults = NULL};
  vkQueuePresentKHR(pApp->presentQueue, &presentInfo);
  pApp->currentFrame = (pApp->currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

void createSyncObjects(App *pApp) {
  pApp->imageAvailableSemaphore =
      (VkSemaphore *)malloc(MAX_FRAMES_IN_FLIGHT * sizeof(VkSemaphore));
  pApp->renderFinishedSemaphore =
      (VkSemaphore *)malloc(MAX_FRAMES_IN_FLIGHT * sizeof(VkSemaphore));
  pApp->inFlightFence =
      (VkFence *)malloc(MAX_FRAMES_IN_FLIGHT * sizeof(VkFence));

  VkSemaphoreCreateInfo semaphoreInfo = {
      .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO};
  VkFenceCreateInfo fenceInfo = {.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
                                 .flags = VK_FENCE_CREATE_SIGNALED_BIT};
  for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
    if (vkCreateSemaphore(pApp->device, &semaphoreInfo, NULL,
                          &pApp->imageAvailableSemaphore[i]) != VK_SUCCESS ||
        vkCreateSemaphore(pApp->device, &semaphoreInfo, NULL,
                          &pApp->renderFinishedSemaphore[i]) != VK_SUCCESS ||
        vkCreateFence(pApp->device, &fenceInfo, NULL,
                      &pApp->inFlightFence[i]) != VK_SUCCESS) {
      fprintf(stderr, "Failed to create semaphore!\n");
      abort();
    }
  }
}
