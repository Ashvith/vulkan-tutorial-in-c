CC := gcc
SRCDIR := src
BUILDDIR := build
TARGET := build/main
SRCEXT := c
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
CFLAGS := -std=c2x -g -Ofast
LDFLAGS := -lglfw -lvulkan -ldl -lpthread -lX11 -lXxf86vm -lXrandr -lXi
VFLAGS := -Wall -Werror -Wpedantic -Wconversion -Wcast-align -Wunused -Wshadow -Wpointer-arith -Wcast-qual -Wmissing-prototypes -Wno-missing-braces
INC := -I include

GLSL := glslangValidator
GLSLTARGET := shader
SHADERDIR := shaders
SPIRVEXT := spv
VERT := vert
FRAG := frag
VERTSRC := $(GLSLTARGET).$(VERT)
FRAGSRC := $(GLSLTARGET).$(FRAG)
VERTTARGET := $(BUILDDIR)/$(VERT).$(SPIRVEXT)
FRAGTARGET := $(BUILDDIR)/$(FRAG).$(SPIRVEXT)

pre-build:
	@echo "Creating build/ directory..."
	@mkdir -p $(BUILDDIR)

compile-all: pre-build $(GLSLTARGET) $(TARGET)
	@echo "Compilation of application and shaders completed!"

compile-shader: pre-build $(GLSLTARGET)
	@echo "Compilation of shaders completed!"

compile-vertex-shader: pre-build $(VERTTARGET)
	@echo "Compilation of vertex shaders completed!"

compile-fragment-shader: pre-build $(FRAGTARGET)
	@echo "Compilation of fragment shaders completed!"

compile-app: pre-build $(TARGET)
	@echo "Compilation of application completed!"

run: compile-all
	@echo "Starting application..."
	./$(TARGET)

clean:
	@echo "Removing build/ directory..."
	rm -rf ./$(BUILDDIR)

$(VERTTARGET): $(SHADERDIR)/$(VERTSRC)
	@echo "Compiling vertex shader..."
	$(GLSL) -V $^ -o $(VERTTARGET)

$(FRAGTARGET): $(SHADERDIR)/$(FRAGSRC)
	@echo "Compiling fragment shader..."
	$(GLSL) -V $^ -o $(FRAGTARGET)

$(GLSLTARGET): $(VERTTARGET) $(FRAGTARGET)
	@echo "Shader compilation complete..."

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@echo "Compiling $< to object file $@"
	$(CC) $(CFLAGS) $(INC) -c -o $@ $< -save-temps

$(TARGET): $(OBJECTS)
	@echo "Linking object file $(OBJECTS) to executable $(TARGET)"
	$(CC) $^ -o $(TARGET) $(LIB) $(VFLAGS) $(LDFLAGS)

.PHONY: pre-build compile-all compile-shader compile-app run clean
.DEFAULT_GOAL := compile-all
